import type { IPost } from "@/interfaces/IPosts";
const PostData: IPost[] = [
    
    {
        id:1,
        title:'Labore fugiat',
        body: 'Lorem ipsuuuuuuuuuuuuuuuuuum',
        date: new Date()
    
    },
    {
        id:2,
        title:'Labore fugiat',
        body: 'eque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci veli',
        date: new Date()
    
    },
    {
        id:3,
        title:'Labore fugiat',
        body: 'mentum, malesuada a diam. Vivamus mattis ex lectus, eu f',
        date: new Date()
    
    },
    {
        id:4,
        title:'Do Lorem eu eiusmod.',
        body: 'Etiam eu sodales odio. Mauris urna tortor, ornare eget iaculis in, auctor sed justo. Cras vestibulum arcu lacus, sed condimentum quam pretium ut. Etiam tincidunt sapien lectus, ac i',
        date: new Date()
    
    },
    {
        id:5,
        title:'Minim id culpa sunt sint officia.',
        body: 'dio. In laoreet lectus ac sapien aliquet, nec tristique orci posuere. Integer molestie elit tincidunt sapien sodales'
        ,
        date: new Date()
    
    }

];

export default PostData